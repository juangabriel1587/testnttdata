	FROM openjdk:8-jdk-slim
	COPY "./target/microservicio-clientes-0.0.1-SNAPSHOT.jar" "app.jar"
	EXPOSE 8090
	ENTRYPOINT ["java","-jar","app.jar"]