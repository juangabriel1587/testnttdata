package com.microservicioclientes.dto;

import java.io.Serializable;
import java.util.List;

public class ReporteDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ClienteDto cliente;
	private List<CuentaDto> cuentas;
	public ClienteDto getCliente() {
		return cliente;
	}
	public void setCliente(ClienteDto cliente) {
		this.cliente = cliente;
	}
	public List<CuentaDto> getCuentas() {
		return cuentas;
	}
	public void setCuentas(List<CuentaDto> cuentas) {
		this.cuentas = cuentas;
	}
	
	

}
