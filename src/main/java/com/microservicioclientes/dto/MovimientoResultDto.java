package com.microservicioclientes.dto;

import java.io.Serializable;


public class MovimientoResultDto implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private java.util.Date Fecha;
	private String Cliente;
	private String NumeroCuenta;
	private String Tipo;
	private String SaldoInicial;
	private String Estado;
	private String Movimiento;
	private String SaldoDisponible;
	private String TipoMovimiento;
	
	
	
	
	
	public MovimientoResultDto() {
		
	}
	
	
	
	public String getTipoMovimiento() {
		return TipoMovimiento;
	}



	public void setTipoMovimiento(String tipoMovimiento) {
		TipoMovimiento = tipoMovimiento;
	}



	public java.util.Date getFecha() {
		return Fecha;
	}

	public void setFecha(java.util.Date fecha) {
		Fecha = fecha;
	}

	public String getCliente() {
		return Cliente;
	}
	public void setCliente(String cliente) {
		Cliente = cliente;
	}
	public String getNumeroCuenta() {
		return NumeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		NumeroCuenta = numeroCuenta;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public String getSaldoInicial() {
		return SaldoInicial;
	}
	public void setSaldoInicial(String saldoInicial) {
		SaldoInicial = saldoInicial;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getMovimiento() {
		return Movimiento;
	}
	public void setMovimiento(String movimiento) {
		Movimiento = movimiento;
	}
	public String getSaldoDisponible() {
		return SaldoDisponible;
	}
	public void setSaldoDisponible(String saldoDisponible) {
		SaldoDisponible = saldoDisponible;
	}
	
	
	
}
