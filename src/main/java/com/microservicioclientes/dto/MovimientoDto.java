package com.microservicioclientes.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;



public class MovimientoDto {

	private Long id;
	private Date fecha;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String tipoMovimiento;
	private Double valor;
	private Double saldo;
	private Long cuentaId;
	private CuentaDto cuenta;
	
	
	
	

	
	public CuentaDto getCuenta() {
		return cuenta;
	}
	public void setCuenta(CuentaDto cuenta) {
		this.cuenta = cuenta;
	}
	public Long getCuentaId() {
		return cuentaId;
	}
	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	
}
