package com.microservicioclientes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket v1APIConfiguration() {
        return new Docket(
                DocumentationType.SWAGGER_2).groupName("clientes").select()
                .apis(RequestHandlerSelectors.basePackage("com.microservicioclientes.controllers"))
                .paths(PathSelectors.regex("/api.*"))
                .build()
                .apiInfo(new ApiInfoBuilder().version("1.0")
                        .title("Banco Pichincha Test Service")
                        .description("\uD83C\uDDEA\uD83C\uDDE8 Implementanción Test NTTDATA")
                        .build());
	}
	
	
}
