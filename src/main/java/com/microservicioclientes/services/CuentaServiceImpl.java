package com.microservicioclientes.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.microservicioclientes.dto.CuentaDto;
import com.microservicioclientes.models.dao.IClienteDao;
import com.microservicioclientes.models.dao.ICuentaDao;
import com.microservicioclientes.models.entity.Cliente;
import com.microservicioclientes.models.entity.Cuenta;

@Service
public class CuentaServiceImpl implements ICuentaService {

	@Autowired
	private  ICuentaDao cuentaDao;
	@Autowired
	private IClienteDao clienteDao;
	@Autowired
	private ModelMapper modelMapper;
	
	
	
	@Override
	@Transactional(readOnly = true)
	public List<CuentaDto> findAll() {
		// TODO Auto-generated method stub
		List<Cuenta> cuentaEntityLs = cuentaDao.findAll();
		return cuentaEntityLs.stream().map(cuenta->modelMapper.map(cuenta, CuentaDto.class)).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public CuentaDto finfById(Long id) {
		// TODO Auto-generated method stub	
		Cuenta cuenta = cuentaDao.findById(id).orElse(null);
		return modelMapper.map(cuenta, CuentaDto.class);
	}

	@Override
	@Transactional
	public CuentaDto save(CuentaDto cuenta) {
		// TODO Auto-generated method stub
		Cuenta cuentaN = new Cuenta();//.save(modelMapper.map(cuenta, Cuenta.class));
		Cliente clin= clienteDao.findById(cuenta.getClienteId()).orElse(null);
		cuentaN.setCliente(clin);
		cuentaN.setEstado(cuenta.getEstado());
		cuentaN.setNumeroCuenta(cuenta.getNumeroCuenta());
		cuentaN.setSaldoInicial(cuenta.getSaldoInicial());
		cuentaN.setTipoCuenta(cuenta.getTipoCuenta());
		cuentaN.setLimiteDiario(cuenta.getLimiteDiario());
		Cuenta ci= cuentaDao.save(cuentaN);
		return modelMapper.map(ci, CuentaDto.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		cuentaDao.deleteById(id);
	}

	@Override
	public List<CuentaDto> finByClienteId(Long clienteId) {
		// TODO Auto-generated method stub
		List<Cuenta> cuentasCliente = cuentaDao.finByCuentaQuery(clienteId);
		return cuentasCliente.stream().map(cuenta->modelMapper.map(cuenta, CuentaDto.class)).collect(Collectors.toList());
	}

	

}
