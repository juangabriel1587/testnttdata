package com.microservicioclientes.services;

import java.util.List;

import com.microservicioclientes.dto.ClienteDto;

public interface IClienteService {

	
	public List<ClienteDto> findAll();
	
	public ClienteDto save(ClienteDto cliente);
	
	public ClienteDto findById(Long id);
	
	public void delete(Long id);
	
}
