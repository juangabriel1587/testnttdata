package com.microservicioclientes.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.microservicioclientes.dto.CuentaDto;
import com.microservicioclientes.dto.MovimientoDto;
import com.microservicioclientes.dto.MovimientoResultDto;
import com.microservicioclientes.services.ICuentaService;
import com.microservicioclientes.services.MovimientoServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(description = "Gestiona ciclo de vida de movimientos")
public class MovimientosRestController {

	@Autowired
	private MovimientoServiceImpl movimientoService;
	@Autowired
	private ICuentaService cuentaService;
	
	
	@GetMapping(value = "/movimientos")
	@ApiOperation(value = "Lista de movimientos")
	public ResponseEntity<?> index(){
		
		Map<String, Object> response = new HashMap<>();
		try {
			List<MovimientoDto> movimientoList = movimientoService.findAll();
			List<MovimientoResultDto> listaMovimientos= new ArrayList<>();
			
			movimientoList.forEach(movimiento->{
				//CuentaDto  cuenta = movimiento.getCuenta().getNumeroCuenta();
				MovimientoResultDto mov = new MovimientoResultDto();
				mov.setCliente(movimiento.getCuenta().getCliente().getNombre());
				mov.setEstado(movimiento.getCuenta().getEstado().toString());
				mov.setFecha(movimiento.getFecha());
				mov.setMovimiento(movimiento.getValor().toString());
				mov.setSaldoDisponible(movimiento.getSaldo().toString());
				mov.setNumeroCuenta(movimiento.getCuenta().getNumeroCuenta());
				mov.setSaldoInicial(movimiento.getCuenta().getSaldoInicial().toString());
				mov.setTipo(movimiento.getCuenta().getTipoCuenta());
				listaMovimientos.add(mov);
				
			});
			
			return new ResponseEntity<List<MovimientoResultDto>>(listaMovimientos,HttpStatus.OK);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al consultar movimientos");
			response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@GetMapping("/movimientos/{id}")
	@ApiOperation(value = "Obtiene movimiento por id")
	public ResponseEntity<?> show(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		try {
			MovimientoDto movimiento = movimientoService.findById(id);
			if(movimiento==null) {
				response.put("messagge", "Movimiento con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			MovimientoResultDto mov = new MovimientoResultDto();
			mov.setCliente(movimiento.getCuenta().getCliente().getNombre());
			mov.setEstado(movimiento.getCuenta().getEstado().toString());
			mov.setFecha(movimiento.getFecha());
			mov.setMovimiento(movimiento.getValor().toString());
			mov.setSaldoDisponible(movimiento.getSaldo().toString());
			mov.setNumeroCuenta(movimiento.getCuenta().getNumeroCuenta());
			mov.setSaldoInicial(movimiento.getCuenta().getSaldoInicial().toString());
			mov.setTipo(movimiento.getCuenta().getTipoCuenta());
			
			
			return new ResponseEntity<MovimientoResultDto>(mov,HttpStatus.OK);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al obtener movimiento");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	

	
	@PostMapping("/movimientos")
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "Crear movimiento")
	public ResponseEntity<?> create(@Valid @RequestBody MovimientoDto movimiento, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		try {
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(System.currentTimeMillis());
			String datenow =  formatter.format(date);
			
			// validar numero de cuenta
			CuentaDto cuentaV = cuentaService.finfById(movimiento.getCuentaId());
			if(cuentaV==null) {
				response.put("messagge", "El número de cuenta con ID: ".concat(movimiento.getCuentaId().toString().concat(" NO existe en la base de datos!")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			
			Double valorDiarioINgresado = movimientoService.valorDiario(movimiento.getCuentaId(),datenow);
			
			// valida cupo diario 
			MovimientoDto ultimoMovimiento = movimientoService.obtenerUltimoRegistro(movimiento.getCuentaId());
			if(movimiento.getTipoMovimiento().equals("CREDITO")) {
				Double saldo = ultimoMovimiento==null? cuentaV.getSaldoInicial() + movimiento.getValor() :ultimoMovimiento.getSaldo() + movimiento.getValor();
				movimiento.setSaldo(saldo);
			}
			if(movimiento.getTipoMovimiento().equals("DEBITO")) {
				
				if(movimiento.getValor()>(ultimoMovimiento==null?0:ultimoMovimiento.getSaldo())) {
					response.put("messagge", "Saldo no disponible!");
					return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
				}
				
				if(cuentaV.getLimiteDiario()<valorDiarioINgresado) {
					response.put("messagge", "Cupo Diario Excedido");
					return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
				}
				
				Double saldo = ultimoMovimiento==null? 0 :ultimoMovimiento.getSaldo() - movimiento.getValor();
				movimiento.setSaldo(saldo);
			}
			
			MovimientoDto movimientoN = movimientoService.save(movimiento);
			return new ResponseEntity<MovimientoDto>(movimientoN,HttpStatus.CREATED);
		} catch (DataAccessException e) {
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		//return clienteService.save(cliente);
	}
	
	
	
	
	@PutMapping("/movimientos/{id}")
	@ApiOperation(value = "Actualizar movimiento por ID")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<?> update(@Valid @RequestBody MovimientoDto movimentoUpdate ,@PathVariable Long id,BindingResult result){
		Map<String, Object> response = new HashMap<>();
		
		
		try {
			MovimientoDto movimiento = movimientoService.findById(id);
			if(movimiento==null) {
				response.put("messagge", "Movimiento con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			
			movimiento.setSaldo(movimentoUpdate.getSaldo());
			movimiento.setTipoMovimiento(movimentoUpdate.getTipoMovimiento());
			movimiento.setValor(movimentoUpdate.getValor());
			//cuenta.set
			MovimientoDto movimientoU = movimientoService.save(movimiento);
			return new ResponseEntity<MovimientoDto>(movimientoU,HttpStatus.CREATED);
			
			
			
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

		
	}
	
	
	@GetMapping("/movimientos/cliente/{clienteId}/{fechaDesde}/{fechaHasta}")
	@ApiOperation(value = "consutal movimientos por cliente y fechas")
	public ResponseEntity<?> showByClienteId(@PathVariable Long clienteId,@PathVariable String fechaDesde ,@PathVariable String fechaHasta){
		
		Map<String, Object> response = new HashMap<>();
		try {
			List<CuentaDto> cuentasLis = cuentaService.finByClienteId(clienteId);
			List<MovimientoResultDto> movimientosList= new ArrayList<>();
			cuentasLis.forEach(cuenta->{
				//CuentaDto  cuenta = movimiento.getCuenta().getNumeroCuenta();
				List<MovimientoDto>  movls = movimientoService.findByCuentaId(cuenta.getId(),fechaDesde,fechaHasta);
				if(movls.size()>0) {
					movls.forEach(movimiento->{
						MovimientoResultDto mov = new MovimientoResultDto();
						mov.setCliente(movimiento.getCuenta().getCliente().getNombre());
						mov.setEstado(movimiento.getCuenta().getEstado().toString());
						mov.setFecha(movimiento.getFecha());
						mov.setMovimiento(movimiento.getTipoMovimiento().equals("DEBITO")? "-".concat(movimiento.getValor().toString()): movimiento.getValor().toString());
						mov.setSaldoDisponible(movimiento.getSaldo().toString());
						mov.setNumeroCuenta(movimiento.getCuenta().getNumeroCuenta());
						mov.setSaldoInicial(movimiento.getCuenta().getSaldoInicial().toString());
						mov.setTipo(movimiento.getCuenta().getTipoCuenta());
						mov.setTipoMovimiento(movimiento.getTipoMovimiento());
						movimientosList.add(mov);
					});
				}
				
				
			});
			
			
			
			return new ResponseEntity<List<MovimientoResultDto>>(movimientosList,HttpStatus.OK);	
		}catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al consultar movimientos");
			response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/movimientos/{id}")
	@ApiOperation(value = "Borrar movimiento por ID")
	public  ResponseEntity<?> delete(@PathVariable Long id)
	{
		Map<String, Object> response = new HashMap<>();
		try {
			movimientoService.delete(id);
			response.put("mensaje", "Movimiento eliminado con éxito!");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar movimiento de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
