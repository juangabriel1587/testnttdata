package com.microservicioclientes.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.microservicioclientes.dto.ClienteDto;
import com.microservicioclientes.dto.ClienteDto.EstadoCliente;
import com.microservicioclientes.services.IClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/api")
@Api(description = "Gestiona ciclo de vida de clientes")
public class ClienteRestController {
	
	@Autowired
	private IClienteService clienteService;
	
	
	
	@GetMapping("/clientes")
	@ApiOperation(value = "Lista de clientes")
	public ResponseEntity<?> index() throws Exception{
		
		//List<ClienteDto> clientesLis= null;
		Map<String, Object> response = new HashMap<>();
		try {
			List<ClienteDto> clientesLis = clienteService.findAll();
			return new ResponseEntity<List<ClienteDto>>(clientesLis,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response.put("messagge", "Error al consultar clientes");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		//return clienteService.findAll();
	}
	
	
	@GetMapping("/clientes/{id}")
	@ApiOperation(value = "Obtiene Cliente por id")
	public ResponseEntity<?> show(@PathVariable Long id) throws Exception{
		
		
		Map<String, Object> response = new HashMap<>();
		try {
			ClienteDto cliente = clienteService.findById(id);
			if(cliente==null) {
				response.put("messagge", "Cliente con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<ClienteDto>(cliente,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response.put("messagge", "Error al obtener el cliente");
			response.put("error", e.getMessage().toString());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
	}
	
	@PostMapping("/clientes")
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "Crear nuevo cliente")
	public ResponseEntity<?> create(@Valid @RequestBody ClienteDto cliente, BindingResult result) throws Exception{
		Map<String, Object> response = new HashMap<>();
		try {
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			
			ClienteDto clienteN = clienteService.save(cliente);
			return new ResponseEntity<ClienteDto>(clienteN,HttpStatus.CREATED);
		} catch (Exception e) {
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		//return clienteService.save(cliente);
	}
	
	
	@PutMapping("/clientes/{id}")
	@ApiOperation(value = "Actualizar cliente por ID")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<?> update(@Valid @RequestBody ClienteDto clienteUpdate ,@PathVariable Long id,BindingResult result) throws Exception{
		Map<String, Object> response = new HashMap<>();
		if(id==null) {
			response.put("messagge", "ID de cliente  es requerido");
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		try {
			ClienteDto cliente = clienteService.findById(id);
			if(cliente==null) {
				response.put("messagge", "Cliente con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			cliente.setContrasena(clienteUpdate.getContrasena());
			cliente.setDireccion(clienteUpdate.getDireccion());
			cliente.setEdad(clienteUpdate.getEdad());
			cliente.setEstado(clienteUpdate.getEstado());
			cliente.setGenero(clienteUpdate.getGenero());
			cliente.setIdentificacion(clienteUpdate.getIdentificacion());
			cliente.setNombre(clienteUpdate.getNombre());
			cliente.setTelefono(clienteUpdate.getTelefono());
			cliente.setUsuario(cliente.getUsuario());
			
			ClienteDto clienteUp = clienteService.save(cliente);
			
			return new ResponseEntity<ClienteDto>(clienteUp,HttpStatus.CREATED);
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

		
	}
	
	
	@DeleteMapping("/clientes/{id}")
	@ApiOperation(value = "Borrar cliente por ID")
	public  ResponseEntity<?> delete(@PathVariable Long id) throws Exception
	{
		Map<String, Object> response = new HashMap<>();
		try {
			clienteService.delete(id);
			response.put("mensaje", "Cliente eliminado con éxito!");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			response.put("mensaje", "Error al eliminar el cliente de la base de datos");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

	
	@GetMapping("/clientes/reporte/{id}")
	public ResponseEntity<?> reporter(@PathVariable Long idCliente){
		
		
		return null;
	}
}
