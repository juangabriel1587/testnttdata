package com.microservicioclientes.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservicioclientes.models.entity.Cliente;


public interface IClienteDao extends JpaRepository<Cliente, Long> {

	
	
}
