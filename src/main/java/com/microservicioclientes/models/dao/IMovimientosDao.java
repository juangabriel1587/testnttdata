package com.microservicioclientes.models.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.microservicioclientes.dto.MovimientoDto;
import com.microservicioclientes.models.entity.Cuenta;
import com.microservicioclientes.models.entity.Movimientos;

public interface IMovimientosDao extends JpaRepository<Movimientos, Long> {

	

	@Query(value = "select m.* from movimientos m where m.cuenta_id=?1 and m.fecha_movimiento BETWEEN ?2 and ?3",nativeQuery=true)
	public List<Movimientos> finByCuentaQuery(Long cuentaId,String fechadesde,String fechahasta);
	
	@Query(value = "select sum(m.valor) from movimientos m where m.cuenta_id=?1 and m.fecha_movimiento=?2 and m.tipo_movimiento='DEBITO'",nativeQuery=true)
	public Double  finByLImiteHoyQuery(Long cuentaId,String hoy);
	
	@Query(value = "select m.* from movimientos m where m.cuenta_id=?1  order by 1 desc limit 1",nativeQuery=true)
	public Movimientos finByLastQuery(Long cuentaId);
}
